<?php namespace App\Models;

use CodeIgniter\Model;



class DailyModel extends Model{
    protected $DB = 'default';

    protected $table = 'weather_daily_forecast_log';

    public function getWeather(){
            $db      = \Config\Database::connect();
            $builder = $db->table('weather_daily_forecast_log');
            $builder->select('*');
            $builder->join('city','city.id = weather_daily_forecast_log.city_id');
            $builder->join('weather_status','weather_status.id = weather_daily_forecast_log.weather_status_id');
            $builder->orderBy('calendar_date', 'DESC');
            $query = $builder->get();
            return $query->getResultArray(); 
        }
    
    public function searchDaily($search){
        $db      = \Config\Database::connect();
        $builder = $db->table('weather_daily_forecast_log');
        $builder->select('*');
        $builder->join('city','city.id = weather_daily_forecast_log.city_id');
        $builder->join('weather_status','weather_status.id = weather_daily_forecast_log.weather_status_id');
        $builder->orderBy('calendar_date', 'DESC');
        $builder->like('city_name', $search);
        $query = $builder->get();
        return $query->getResultArray();
    }
    public function getCity(){
        $db      = \Config\Database::connect();
        $builder = $db->table('city');
        // $builder->select('*');
        // $builder->join('city','city.id = weather_daily_forecast_log.city_id');
        $query = $builder->get();
        return $query->getResultArray(); 
    }

    public function GetKL(){
        $db      = \Config\Database::connect();
        $builder = $db->table('weather_daily_forecast_log');
        $builder->select('*');
        $builder->join('city','city.id = weather_daily_forecast_log.city_id');
        $builder->join('weather_status','weather_status.id = weather_daily_forecast_log.weather_status_id');
        $builder->orderBy('calendar_date', 'DESC');
        $builder->like('city_name', 'Kuala Lumpur');
        $builder->limit(7);
        $query = $builder->get();
        return $query->getResultArray(); 
    }

    public function GetJohor(){
        $db      = \Config\Database::connect();
        $builder = $db->table('weather_daily_forecast_log');
        $builder->select('*');
        $builder->join('city','city.id = weather_daily_forecast_log.city_id');
        $builder->join('weather_status','weather_status.id = weather_daily_forecast_log.weather_status_id');
        $builder->orderBy('calendar_date', 'DESC');
        $builder->like('city_name', 'Johor');
        $builder->limit(7);
        $query = $builder->get();
        return $query->getResultArray(); 
    }

    public function GetPineng(){
        $db      = \Config\Database::connect();
        $builder = $db->table('weather_daily_forecast_log');
        $builder->select('*');
        $builder->join('city','city.id = weather_daily_forecast_log.city_id');
        $builder->join('weather_status','weather_status.id = weather_daily_forecast_log.weather_status_id');
        $builder->orderBy('calendar_date', 'DESC');
        $builder->like('city_name', 'Pulau Pinang');
        $builder->limit(7);
        $query = $builder->get();
        return $query->getResultArray(); 
    }

}


