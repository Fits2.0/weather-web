<?php namespace App\Models;

use CodeIgniter\Model;



class HourlyModel extends Model{
    protected $DB = 'default';

    protected $table = 'weather_hourly_forecast_log';

    public function getHourly(){
            $db      = \Config\Database::connect();
            $builder = $db->table('weather_hourly_forecast_log');
            $builder->select('*');
            $builder->join('city','city.id = weather_hourly_forecast_log.city_id');
            $builder->join('weather_status','weather_status.id = weather_hourly_forecast_log.weather_status_id');
            $builder->orderBy('forecast_hour', 'DESC');
            $builder->limit(24);
            $query = $builder->get();
            return $query->getResultArray(); 
        }
    
    public function searchHourly($search){
        $db      = \Config\Database::connect();
        $builder = $db->table('weather_hourly_forecast_log');
        $builder->select('*');
        $builder->join('city','city.id = weather_hourly_forecast_log.city_id');
        $builder->join('weather_status','weather_status.id = weather_hourly_forecast_log.weather_status_id');
        $builder->like('city_name', $search);
        $builder->orderBy('forecast_hour', 'DESC');
        $builder->limit(24);
        $query = $builder->get();
        return $query->getResultArray();
    }

    public function getLatestHourly(){
        $db      = \Config\Database::connect();
        $builder = $db->table('weather_hourly_forecast_log');
        $builder->select('*');
        $builder->join('city','city.id = weather_hourly_forecast_log.city_id');
        $builder->join('weather_status','weather_status.id = weather_hourly_forecast_log.weather_status_id');
        $builder->orderBy('forecast_hour', 'DESC');
        $builder->limit(1);
        $query = $builder->get();
        return $query->getResultArray(); 
    }
    public function getCity(){
        $db      = \Config\Database::connect();
        $builder = $db->table('city');
        // $builder->select('*');
        // $builder->join('city','city.id = weather_daily_forecast_log.city_id');
        $query = $builder->get();
        return $query->getResultArray(); 
    }
    public function getWeatherStatus(){
        $db      = \Config\Database::connect();
        $builder = $db->table('weather_status');
        $query = $builder->get();
        return $query->getResultArray(); 
    }


    }


