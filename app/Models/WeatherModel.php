<?php  namespace App\Models;

use CodeIgniter\Model;

class WeatherModel extends Model
{
    public function getCurrentWeather($city,$key){

        $url = 'api.openweathermap.org/data/2.5/weather?id='.$city.'&units=metric&appid='.$key.'';//prep url to call api 
        
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$url);//set curl option
        curl_setopt($ch,CURLOPT_POST, false);//set curl option
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);//set curl option
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);//set curl option

        $result = curl_exec($ch);//execute curl

        $data = json_decode($result);//decode json data


        if(isset($data))
        {
            $data= $this->insertCurrent($city,$data);//insert data into the database weather current
            return $data;// return response from the insert process
        }
        

    }

    public function getHourlyWeather($city,$key){//get hourly weather forecast  MF
        $lat = $this->getLat($city)->getResultObject();
        
        $url = 'https://api.openweathermap.org/data/2.5/onecall?lat='.$lat[0]->latitude.'&lon='.$lat[0]->longitude.'&units=metric&exclude=minutely,current,daily,alerts&appid='.$key.'';//prep url
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);//set curl option
        curl_setopt($ch,CURLOPT_POST, false);//set curl option
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);//set curl option
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);//set curl option

        $result = json_decode(curl_exec($ch),true);//return decoded response as array
        if(isset($result)){//check if result is not empty
            return $this->insertHourly($city,$result);//return result of insert data to hourly table
            // return $result;//result of api call
        }else{
            return curl_error($ch);//return curl error
        }
        curl_close($ch);//close curl 


    }

    public function getDailyWeather($city,$key){//get daily weather from api
        $lat = $this->getLat($city)->getResultObject();//get lat and lon to prep url
        
        $url = 'https://api.openweathermap.org/data/2.5/onecall?lat='.$lat[0]->latitude.'&lon='.$lat[0]->longitude.'&units=metric&exclude=hourly,minutely,current,alerts&appid='.$key.'';//prep url
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);//set curl option
        curl_setopt($ch,CURLOPT_POST, false);//set curl option
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);//set curl option
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);//set curl option

        $result = json_decode(curl_exec($ch),true);//return decoded response as array
        curl_close($ch);//close curl request
        if(isset($result)){
            return $this->insertDaily($city,$result);
            // return $result;//result of api call
        }else{
            return curl_error($ch);
        }

    }
    public function getLat($city){ //get Latitude and Longtitude from database
        $db = \Config\Database::connect();
        $builder =$db->table('city');//condition
        $builder->where('id',$city);//condition
        $result = $builder->get();//run query
        if($result){
            return $result;//return result
        }else{
            return $db->error();//return error
        }
    }
    public function getCity($lat,$lon){//get city from database from lat and lon
        $db = \Config\Database::connect();//call database config
        $builder =$db->table('city');//select table
        $builder->where('longitude',$lon);//condition 1
        $builder->where('latitude',$lat);//condition 2
        $data = $builder->get();//run query
        if($data){//check if query not fail
            return $data;//return data fetched from db

        }else{
            return $db->error();//return error
        }
    }

    public function changeEpoch($epoch){//change epoch time to custom format
        return date('d-m-Y H:i:s',$epoch);
    }

    public function insertDaily($city,array $data){//insert data into daily table
        $daily = $data['daily'];//initialize array 
        $db = \Config\Database::connect();//database config
        $builder = $db->table('weather_daily_forecast_log');//select table
        $status = 'Not started';//initial status
        for($i=0;$i<count($daily);$i++){//condition for loop based on count of the array
            $date = $this->changeEpoch($daily[$i]['dt']);//Date for the forecast
            $weather_id = $daily[$i]['weather'][0]['id'];//Weather ID
            $temp = $daily[$i]['temp']['day'];//temperature
            $tempmin = $daily[$i]['temp']['min'];//Min temperature
            $tempmax = $daily[$i]['temp']['max'];//Max temperature
            $pressure = $daily[$i]['pressure'];//Pressure of the forecast
            $humidity = $daily[$i]['humidity'];//humidity forecast
            $sunrise = $this->changeEpoch($daily[$i]['sunrise']);//sunrise time forecast
            $sunset = $this->changeEpoch($daily[$i]['sunset']);//sunset time forecast


            $builder->set('city_id',$city);//prep data 
            $builder->set('calendar_date',$date);//prep data 
            $builder->set('weather_status_id',$weather_id);//prep data 
            $builder->set('temperature',$temp);//prep data 
            $builder->set('min_temperature',$tempmin);//prep data 
            $builder->set('max_temperature',$tempmax);//prep data 
            $builder->set('pressure',$pressure);//prep data 
            $builder->set('humidity',$humidity);//prep data 
            $builder->set('sunrise_time',$sunrise);//prep data 
            $builder->set('sunset_time',$sunset);//prep data 
            if($builder->insert()){//insert data into database weather_daily_forecast_log
                $status = 'Succesfully updated daily forecast';//return ok if no error
            }else{
                $status = $db->error();//return error
            }

        }
        return $status;//return status of the function
    }

    public function insertHourly($city,array $data){//insert hourly forecast into table weather_hourly_forecast_log
        $hourly = $data['hourly'];//prep array
        $db = \Config\Database::connect();//db config
        $builder =$db->table('weather_hourly_forecast_log');//table name
        $status = 'Not Started';//initial status
        $x = count($hourly);//count for loop
        for($i = 0;$i<$x;$i++){//loop
            $hour = $this->changeEpoch($hourly[$i]['dt']);//hour forecasted
            $weather_id = $hourly[$i]['weather'][0]['id'];//weather id
            $temp = $hourly[$i]['temp'];//temperature
            $pressure = $hourly[$i]['pressure'];//forecasted pressure
            $humidity = $hourly[$i]['humidity'];//forecasted humnidity
            $windspeed = $hourly[$i]['wind_speed'];//forecasted wind speed

            $builder->set('city_id',$city);//prep data 
            $builder->set('forecast_hour',$hour);//prep data 
            $builder->set('weather_status_id',$weather_id);//prep data 
            $builder->set('temperature',$temp);//prep data 
            $builder->set('pressure',$pressure);//prep data 
            $builder->set('humidity',$humidity);//prep data 
            $builder->set('windspeed',$windspeed);//prep data 
            if($builder->insert()){//insert data into database
                $status = 'Successfully updated hourly forecast';//return status
            }else{
                $status = $db->error();//error status
            }
        }
        return $status;//return status
    }
        
    public function insertCurrent($city,object $data){//iinsert current weather into database

        $weather_id = $data->weather[0]->id;//weather id
        $temp = $data->main->temp;//Temperature
        $tempmin = $data->main->temp_min;//Min temperature
        $tempmax = $data->main->temp_max;//max temperature
        $pressure = $data->main->pressure;//Pressure
        $humidity = $data->main->humidity;//humidity
        $sunset = $this->changeEpoch($data->sys->sunset);//sunset time
        $sunrise = $this->changeEpoch($data->sys->sunrise);//sunrise time

        $db = \Config\Database::connect();//db config
        $builder =$db->table('weather_current');//db table
        $builder->set('city_id',$city);//prep data 
        $builder->set('weather_status_id',$weather_id);//prep data 
        $builder->set('temperature',$temp);//prep data 
        $builder->set('min_temperature',$tempmin);//prep data 
        $builder->set('max_temperature',$tempmax);//prep data 
        $builder->set('pressure',$pressure);//prep data 
        $builder->set('humidity',$humidity);//prep data 
        $builder->set('sunrise_time',$sunrise);//prep data 
        $builder->set('sunset_time',$sunset);//prep data 
        // $builder->set('last_updated_at');
        

        if($builder->insert()){//run insert query
            return 'Successfully updated current weather';//return success
        }else{
            return $db->error();//return error
        }
    }

    public function getLastHourForecast(){//return the last data in hourly forecast
        $db = \Config\Database::connect();//db config
        $builder =$db->table('weather_hourly_forecast_log');//db table
        $builder->select('forecast_hour');
        $builder->orderBy('forecast_hour','DESC');
        $builder->limit(1);
        return $builder->get()->getResultObject();
    }

    function getLastDailyForecast(){
        $db = \Config\Database::connect();//db config
        $builder =$db->table('weather_daily_forecast_log');//db table
        $builder->select('calendar_date');
        $builder->orderBy('calendar_date','DESC');
        $builder->limit(1);
        return $builder->get()->getResultObject();
    }



}

