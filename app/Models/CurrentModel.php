<?php namespace App\Models;

use CodeIgniter\Model;



class CurrentModel extends Model{
    protected $DB = 'default';

    protected $table = 'weather_current';

    public function getCurrentWeather(){
        $db      = \Config\Database::connect();
        $builder = $db->table('weather_current');
        $builder->select('*');
        $builder->join('city','city.id = weather_current.city_id');
        $builder->join('weather_status','weather_status.id = weather_current.weather_status_id');
        $builder->orderBy('last_updated_at', 'DESC');//sort data descending
        $builder->limit(3);
        $query = $builder->get();//run select query
        return $query->getResultArray(); //return result as array
    }
    
    public function searchCurrent($search){
        $db      = \Config\Database::connect();
        $builder = $db->table('weather_current');
        $builder->select('*');
        $builder->join('city','city.id = weather_current.city_id');
        $builder->join('weather_status','weather_status.id = weather_current.weather_status_id');
        $builder->orderBy('last_updated_at', 'DESC');
        $builder->like('city_name', $search);
        $builder->limit(1);
        $query = $builder->get();
        return $query->getResultArray();
    }

    public function getCity(){
        $db      = \Config\Database::connect();
        $builder = $db->table('city');
        // $builder->select('*');
        // $builder->join('city','city.id = weather_current.city_id');
        $query = $builder->get();
        return $query->getResultArray(); 
    }

    public function getWeatherStatus(){
        $db      = \Config\Database::connect();
        $builder = $db->table('weather_status');
        $query = $builder->get();
        return $query->getResultArray(); 
    }
    }


