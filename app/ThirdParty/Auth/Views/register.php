<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Login</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?= base_url('fonts/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?= base_url('style.css') ?>">
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>
	<body>

<?= $this->extend($config->viewLayout) ?>
<?= $this->section('main') ?>
<div class="site-content"  style = "min-height: 100vh;">
            <?php include 'include/login/header.php';?>  <!-- .site-header -->
            <main class="main-content">
            <div class="fullwidth-block">
                <div class="container">	
<h1><?= lang('Auth.registration') ?></h1>

<?= view('Auth\Views\_notifications') ?>

<form method="POST" action="<?= base_url('register'); ?>" accept-charset="UTF-8"
	onsubmit="registerButton.disabled = true; return true;">
	<?= csrf_field() ?>
	<p>
	    <label><?= lang('Auth.name') ?></label><br />
	    <input required minlength="2" type="text" name="name" value="<?= old('name') ?>" />
	</p>
	<p>
	    <label><?= lang('Auth.email') ?></label><br />
	    <input required type="email" name="email" value="<?= old('email') ?>" />
	</p>
	<p>
		<label><?= lang('City') ?></label><br />
		<select name="city" id="city">
			<option value="1733046">Kuala Lumpur</option>
			<option value="1733047">Pulau Pinang</option>
			<option value="1733049">Johor</option>
		</select>
	</p>
	<p>
	    <label><?= lang('Auth.password') ?></label><br />
	    <input required minlength="5" type="password" name="password" value="" />
	</p>
	<p>
	    <label><?= lang('Auth.passwordAgain') ?></label><br />
	    <input required minlength="5" type="password" name="password_confirm" value="" />
	</p>
	<p>
	    <button name="registerButton" type="submit"><?= lang('Auth.register') ?></button>
	</p>
	<p>
		<a href="<?= base_url('login'); ?>" class="float-right"><?= lang('Auth.alreadyRegistered') ?></a>
	</p>
</form>
</div>
            </main>
        </div>
            
            <?php include 'include/footer/footer.php';?> <!-- .site-footer -->

        <?= $this->endSection() ?>
    </body>
    <script src="<?php base_url('js/jquery-1.11.1.min.js') ?>"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
	<script src="<?php base_url('js/plugins.js') ?>"></script>
	<script src="<?php base_url('js/app.js') ?>"></script>
</html>