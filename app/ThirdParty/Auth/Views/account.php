<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Login</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?= base_url('fonts/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?= base_url('style.css') ?>">
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>
	<body>
<?= $this->extend($config->viewLayout) ?>
<?= $this->section('main') ?>
<div class="site-content"  style = "min-height: 100vh;">
            <?php include 'include/login/header.php';?>  <!-- .site-header -->
            <main class="main-content">
            <div class="fullwidth-block">
                <div class="container">
<h1><?= lang('Auth.accountSettings') ?></h1>

<?= view('Auth\Views\_notifications') ?>

<form method="POST" action="<?= site_url('account'); ?>" accept-charset="UTF-8">
	<?= csrf_field() ?>
	<p>
		<label><?= lang('Auth.name') ?></label><br />
		<input required type="text" name="name" value="<?= $userData['name']; ?>" />
	</p>
	<p>
		<label><?= lang('Auth.email') ?></label><br />
		<input disabled type="text" value="<?= $userData['email']; ?>" />
	</p>
	<?php if ($userData['new_email']): ?>
	<p>
		<label><?= lang('Auth.pendingEmail') ?></label><br />
		<input disabled type="text" value="<?= $userData['new_email']; ?>" />
	</p>
	<p>
		<label><?= lang('City') ?></label><br />
		<select name="city" id="city">
			<option value="1733046">Kuala Lumpur</option>
			<option value="1733047">Pulau Pinang</option>
			<option value="1733049">Johor</option>
		</select>
	</p>
	<?php endif; ?>
    <p>
        <button type="submit"><?= lang('Auth.update') ?></button>
    </p>
</form>


<!-- CHANGE EMAIL -->
<h2><?= lang('Auth.changeEmail') ?></h2>
<p><?= lang('Auth.changeEmailInfo') ?></p>

<form method="POST" action="<?= site_url('change-email'); ?>" accept-charset="UTF-8"
	onsubmit="changeEmail.disabled = true; return true;">
	<?= csrf_field() ?>
	<p>
		<label><?= lang('Auth.newEmail') ?></label><br />
		<input required type="email" name="new_email" value="<?= old('new_email') ?>" />
	</p>
	<p>
		<label><?= lang('Auth.currentPassword') ?></label><br />
		<input required type="password" name="password" value="" />
	</p>
    <p>
        <button name="changeEmail" type="submit"><?= lang('Auth.update') ?></button>
    </p>
</form>


<!-- CHANGE PASSWORD -->
<h2><?= lang('Auth.changePassword') ?></h2>

<form method="POST" action="<?= site_url('change-password'); ?>" accept-charset="UTF-8"
	onsubmit="changePassword.disabled = true; return true;">
	<?= csrf_field() ?>
	<p>
		<label><?= lang('Auth.currentPassword') ?></label><br />
		<input required type="password" minlength="5" name="password" value="" />
	</p>
	<p>
		<label><?= lang('Auth.newPassword') ?></label><br />
		<input required type="password" minlength="5" name="new_password" value="" />
	</p>
	<p>
		<label><?= lang('Auth.newPasswordAgain') ?></label><br />
		<input required type="password" minlength="5" name="new_password_confirm" value="" />
	</p>
    <p>
        <button name="changePassword" type="submit"><?= lang('Auth.update') ?></button>
    </p>
</form>


<!-- DELETE ACCOUNT -->
<h2><?= lang('Auth.deleteAccount') ?></h2>

<form method="POST" action="<?= site_url('delete-account') ?>" accept-charset="UTF-8">
    <?= csrf_field() ?>
    <p><?= lang('Auth.deleteAccountInfo') ?></p>
	<p>
		<label><?= lang('Auth.currentPassword') ?></label><br />
		<input required type="password" name="password" value="" />
	</p>
	<p>
		<button type="submit" onclick="return confirm('<?= lang('Auth.areYouSure') ?>')"><?= lang('Auth.deleteAccount') ?></button>
	</p>
	</form>
</div>
            </main>
        </div>
            
            <?php include 'include/footer/footer.php';?> <!-- .site-footer -->

        <?= $this->endSection() ?>
    </body>
    <script src="<?php base_url('js/jquery-1.11.1.min.js') ?>"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
	<script src="<?php base_url('js/plugins.js') ?>"></script>
	<script src="<?php base_url('js/app.js') ?>"></script>
</html>