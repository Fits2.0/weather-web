<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Login</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?= base_url('fonts/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?= base_url('style.css') ?>">
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>
	<body>

        <?= $this->extend($config->viewLayout) ?>
        <?= $this->section('main') ?>
        <div class="site-content"  style = "min-height: 100vh;">
            <?php include 'include/login/header.php';?>  <!-- .site-header -->
            <main class="main-content">
            <div class="fullwidth-block">
                <div class="container">			
<h1><?= lang('Auth.forgottenPassword') ?></h1>

<?= view('Auth\Views\_notifications') ?>

<form method="POST" action="<?= site_url('forgot-password'); ?>" accept-charset="UTF-8"
	onsubmit="submitButton.disabled = true; return true;">
	<?= csrf_field() ?>
    <p>
        <label><?= lang('Auth.typeEmail') ?></label><br />
        <input required type="email" name="email" value="<?= old('email') ?>" />
    </p>
    <p>
        <button name="submitButton" type="submit"><?= lang('Auth.setNewPassword') ?></button>
    </p>
</form>
</div>
            </main>
        </div>
            
            <?php include 'include/footer/footer.php';?> <!-- .site-footer -->

        <?= $this->endSection() ?>
    </body>
    <script src="<?php base_url('js/jquery-1.11.1.min.js') ?>"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
	<script src="<?php base_url('js/plugins.js') ?>"></script>
	<script src="<?php base_url('js/app.js') ?>"></script>
</html>