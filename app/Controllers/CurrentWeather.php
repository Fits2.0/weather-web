<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\DailyModel;
use App\Models\CurrentModel;


class CurrentWeather extends Controller 
{
	public function index(){
		$model = new CurrentModel();
		$daily = new DailyModel();
		$data['weather'] = $model->getCurrentWeather();
		$data['daily'] = $daily->getWeather();
		echo view('CurrentWeather',$data);
		// echo view('Signup',$data);
	}

	public function fetch(){
		helper('form');
		$query = '';
		$query = $this->request->getVar('search_text');
		$model = new CurrentModel();
		$data['weather'] = $model->searchCurrent($query);
		echo view('CurrentWeather',$data);
	}
}