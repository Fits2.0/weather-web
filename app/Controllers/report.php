<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\DailyModel;

class report extends Controller
{
    public function index()
    {
        $model = new DailyModel();
        $data['weather'] = $model->GetWeather();
        $data['kl'] = $model->GetKL();
        $data['johor'] = $model->GetJohor();
        $data['pineng'] = $model->GetPineng();
        /*if (empty($data['weather']))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find the news item: ');
        }*/
        
        echo view('Report', $data);
        //$session = session();

    }
}