<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\CurrentModel;

class Home extends BaseController
{
	protected $session;

	public function index()
	{	
		$model = new CurrentModel();
		$data['weather'] = $model->getCurrentWeather();
		echo view('Homepage', $data);
	}

	//--------------------------------------------------------------------

}
