<?php namespace App\Controllers;

use CodeIgniter\Controller;

class Current extends Controller
{
    public function index()
    {
        $session = session();
        $data = [
            'email'=> $session->get('email'),
            'name'  => $session->get('name'),
            'city' => $session->get('city')
        ];
        echo view('user/Current', $data);

    }
}