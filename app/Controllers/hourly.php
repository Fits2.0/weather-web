<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\HourlyModel;


class Hourly extends Controller 
{
	
	public function index(){
		$model = new HourlyModel();
		$data['weather'] = $model->getHourly();
		/*if (empty($data['weather']))
		{
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find the news item: ');
		}*/

		echo view('Hourly', $data);
		// echo view('Signup',$data);
	}

	public function fetch(){
		helper('form');
		$query = '';
		$query = $this->request->getVar('search_text');
		$model = new HourlyModel();
		$data['weather'] = $model->searchHourly($query);
		
		echo view('Hourly', $data);
	}
	
}