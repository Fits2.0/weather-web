<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\DailyModel;
use App\Models\WeatherModel;


class Weather extends Controller 
{	
	protected $key = 'a1b56c7b5588c0e0dd4bde0ca84ea5ff';
	protected $key1 = '5de475b4d8bc6d4307518119a74566df';
	protected $city1 = '1733046';//KL
	protected $city2 = '1733047'; //Penang
	protected $city3 = '1733049'; // Johor
	
	public function index(){
		$model = new DailyModel();
		$data['weather'] = $model->getWeather();
		/*if (empty($data['weather']))
		{
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find the news item: ');
		}
		*/
		echo view('Weather',$data);
		// echo view('Signup',$data);
	}

	public function fetch(){
		helper('form');
		$query = '';
		$query = $this->request->getVar('search_text');
		//$query = $this->input->post('search_text');
		$model = new DailyModel();
		$data['weather'] = $model->searchDaily($query);
		//return redirect()->to('../Weather');
		echo view('Weather',$data);
	}



    public function getUpdate($choice){//choice to update
		 $key = 'a1b56c7b5588c0e0dd4bde0ca84ea5ff';
		 $key1 = '5de475b4d8bc6d4307518119a74566df';
		 $city1 = '1733046';//KL
		 $city2 = '1733047'; //Penang
		 $city3 = '1733049'; // Johor
		/*
			1 = return current weather
			2 = return hourly forecast
			3 = return daily forecast
			else = error
		
		*/
		$model = new WeatherModel();//prep weather model
		if($choice == 1){
			$data['t1'] = $model->getCurrentWeather($city1,$key);
			$data['t2'] = $model->getCurrentWeather($city2,$key1);
			$data['t3'] = $model->getCurrentWeather($city3,$key1);
		}elseif($choice == 2){
			$data['t1'] = $model->getHourlyWeather($city1,$key);
			$data['t2'] = $model->getHourlyWeather($city2,$key1);
			$data['t3'] = $model->getHourlyWeather($city3,$key1);
		}elseif($choice == 3){
			$data['t1'] = $model->getDailyWeather($city1,$key);
			$data['t2'] = $model->getDailyWeather($city2,$key);
			$data['t3'] = $model->getDailyWeather($city3,$key);
		}elseif($choice == 4){
			$fore = $model->getLastHourForecast();
			$data['t1'] = strtotime($fore[0]->forecast_hour);		
		}elseif($choice == 5){
			$fore = $model ->getLastDailyForecast();
			$data['t1'] = strtotime($fore[0]->calendar_date);
		}
		else{
			$data['t1'] = $data['t2'] = $data['t3'] = 'Wrong choice!!';
		}
		$data['num']= $choice;

		echo view('Test',$data);
	}

}