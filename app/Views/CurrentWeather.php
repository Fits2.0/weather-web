<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Current Weather</title>
		
		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('fonts/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href=<?php echo base_url('style.css'); ?>>

	</head>


	<body >
	<?php include 'include/logged_in.php';?> <!-- logged in user -->
		<div class="site-content">
		<?php include 'include/weather/header.php';?> <!-- .site-header -->
			<main class="main-content">

				<div class="fullwidth-block">
					<div class="container">
					<?php include 'include/user/navbar.php'; ?> <br>
						<form action = "../current-weather/fetch" method = "POST">
						<?= csrf_field() ?>
							<p><input type="text" name="search_text" id="search_text" placeholder="search by city" class="form-control">
                        	<button type="submit"><i class="fa fa-search"></i></button> </p>
						</form>
						<br>
						<div class="row" id="current" >
							<!-- weather data -->
							<?php echo view('weather/Display'); ?>
							<!-- end of weather data -->
						</div>
					</div>
				</div>
				
			</main> <!-- .main-content -->

			<?php include 'include/footer/footer.php';?> <!-- .site-footer -->
		</div>
		
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		<script>
			function HourlyFunction() {
				var x = document.getElementById("current");
				var y = document.getElementById("hourly");
				if (x.style.display === "none") {
					x.style.display = "block";
					y.style.display = "none";
				} else {
					y.style.display = "block"
					x.style.display = "none";
				}
			}
yle.display = "none";
				} else {
					y.style.display = "block"
					x.style.display = "none";
				}
			}
le.display = "none";
				} else {
					y.style.display = "block"
					x.style.display = "none";
				}
			}
yle.display = "none";
display = "none";
				} else {
					y.style.display = "block"
					x.style.display = "none";
				}
			}
yle.display = "none";
				} else {
					y.style.display = "block"
					x.style.display = "none";
				}
			}
		</script>
	</body>

</html>