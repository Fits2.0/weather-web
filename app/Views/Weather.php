<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Daily Weather</title>
		
		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('fonts/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href=<?php echo base_url('style.css'); ?>>

	</head>


	<body >
	<?php include 'include/logged_in.php';?> <!-- logged in user -->
		<div class="site-content">
		<?php include 'include/weather/header.php';?> <!-- .site-header -->
			<main class="main-content">

				<div class="fullwidth-block">
					<div class="container">
					<?php include 'include/user/navbar.php'; ?> <br>
						<form action = "../weather/fetch" method = "POST">
						<?= csrf_field() ?>
							<p><input type="text" name="search_text" id="search_text" placeholder="search by city" class="form-control">
                        	<button type="submit"><i class="fa fa-search"></i></button> </p>
						</form>
						<br>
						<div class="row" id="current" >
							<!-- start data -->
							<?php
								$location = " ";
								if($weather)
								{
									for($i = 0;$i < count($weather);$i++)
									{	
										//echo $weather[$i]["city_id"];
										//echo current		
										//echo $count[$i]["city_id"];
										$KL = base_url('images/cities/cities-01.jpg');
										echo '
										<div class="col-md-6">
										<div class="photo">';
										if ($weather[$i]["city_id"] == '1733046'){
											echo '<img class="photo-preview photo-detail" src="'.base_url('images/cities/cities-01.jpg').'" height = "100%" >';
										}
										elseif ($weather[$i]["city_id"] == '1733047'){
											echo '<img class="photo-preview photo-detail" src="'.base_url('images/cities/cities-02.jpg').'" height = "100%" >';
										}
										elseif ($weather[$i]["city_id"] == '1733049'){
											echo '<img class="photo-preview photo-detail" src="'.base_url('images/cities/cities-03.jpg').'" height = "100%" >';
                                        }
                                        
                                        
										echo'	
											<div class="photo-details">
											<div class="location">'.$weather[$i]["city_name"].'</div>
											<div class="degree">'.date("jS \of F", strtotime($weather[$i]['calendar_date'])).'</div>
											<div class="num">'.$weather[$i]['temperature'].'<sup>o</sup>C</div>
											<div class="day">'.$weather[$i]["description"].'</div>
                                            <div class="forecast-icon">';
                                            if ($weather[$i]["weather_status_id"] == 200 or 201 or 202 or 210 or 211 or 212 or 221 or 230 or 231 or 232){
                                                echo '<img src="'.base_url('images/icons/thunder.gif').'" alt="" width=50 height=50>
                                                    <br><br><br>
                                                    <p>Suggested clothes: </p>
                                                    <img src="'.base_url('images/icons/raincoat.gif').'" alt="" width=50 height=50>
                                                    <img src="'.base_url('images/icons/umbrella.gif').'" alt="" width=50 height=50>
                                                    <br><br>
                                                    <p>Suggested activities: </p>
                                                    <img src="'.base_url('images/icons/read.png').'" alt="" width=50 height=50>';
                                            }
                                            else if ($weather[$i]["weather_status_id"] == 300 or 301 or 302 or 310 or 311 or 312 or 313 or 314 or 321){
                                                echo '<img src="'.base_url('images/icons/rainyy.gif').'" alt="" width=50 height=50>
													<br><br><br>
													<p>Suggested clothes: </p>
													<img src="'.base_url('images/icons/raincoat.gif').'" alt="" width=50 height=50>
													<img src="'.base_url('images/icons/umbrella.gif').'" alt="" width=50 height=50>
													<br><br>
													<p>Suggested activities: </p>
													<img src="'.base_url('images/icons/read.png').'" alt="" width=50 height=50>';
                                            }
                                            else if ($weather[$i]["weather_status_id"] == 500 or 501 or 502 or 503 or 504 or 511 or 520 or 520 or 521 or 522 or 531){
                                                echo '<img src="'.base_url('images/icons/rainyy.gif').'" alt="" width=50 height=50>
													<br><br><br>
													<p>Suggested clothes: </p>
													<img src="'.base_url('images/icons/raincoat.gif').'" alt="" width=50 height=50>
													<img src="'.base_url('images/icons/umbrella.gif').'" alt="" width=50 height=50>
													<br><br>
													<p>Suggested activities: </p>
													<img src="'.base_url('images/icons/read.png').'" alt="" width=50 height=50>';
                                            }
                                            else if ($weather[$i]["weather_status_id"] == 800){
                                                echo '<img src="'.base_url('images/icons/rainyy.gif').'" alt="" width=50 height=50>
                                                <br><br><br>
                                                <p>Suggested clothes: </p>
                                                <img src="'.base_url('images/icons/tshirt2.gif').'" alt="" width=50 height=50>
                                                <br><br>
                                                <p>Suggested activities: </p>
                                                <img src="'.base_url('images/icons/run.png').'" alt="" width=50 height=50>';
                                            }
											echo '	
											</div>';
											echo'	
											</div>
										</div>
										</div>';
									}
								}
								?>
						</div>
					</div>
				</div>
				
			</main> <!-- .main-content -->

			<?php include 'include/footer/footer.php';?> <!-- .site-footer -->
		</div>
		
		<script src="<?php base_url('js/jquery-1.11.1.min.js'); ?>"></script>
		<script src="<?php base_url('js/plugins.js'); ?>"></script>
		<script src="<?php base_url('js/app.js'); ?>"></script>
	</body>

</html>