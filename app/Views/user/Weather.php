

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Weather It</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('fonts/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?php echo base_url('style.css'); ?>">
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>


	<body>
		
		<div class="site-content">
			<div class="site-header">
				<div class="container">
					<a href="index.html" class="branding">
						<img src="images/logo.png" alt="" class="logo">
						<div class="logo-type">
							<h1 class="site-title">Weather It</h1>
							<small class="site-description">The best weather update for you</small>
						</div>
					</a>

					<!-- Default snippet for navigation -->
					<div class="main-navigation">
						<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
						<ul class="menu">
							<li class="menu-item"><a href="index.html">Home</a></li>
							<li class="menu-item"><a href="news.html">User Account</a></li>
							<!--li class="menu-item"><a href="live-cameras.html">Live cameras</a></li-->
							<li class="menu-item current-menu-item"><a href="photos.html">Weather</a></li>
							<li class="menu-item"><a href="contact.html">Contact</a></li>
						</ul> <!-- .menu -->
					</div> <!-- .main-navigation -->

					<div class="mobile-navigation"></div>

				</div>
			</div> <!-- .site-header -->

			<main class="main-content">
				<div class="container">
					<div class="breadcrumb">
						<a href="index.html">Home</a>
						<span>Suggestion</span>
					</div>
				</div>

				<div class="fullwidth-block">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="photo">
									<div class="photo-preview photo-detail" data-bg-image="images/photo-1.jpg"></div>
									<div class="photo-details">
									<div class="location">Tampines State</div>
									<div class="day">Monday</div>
									<div class="date">6 Oct</div>
									<div class="degree">
									<div class="num">23<sup>o</sup>C</div>
									<div class="forecast-icon">
										<img src="images/icons/icon-1.svg" alt="" width=50>
									</div>	
									</div>
										<p>please wear something appropiate based on the Weather today.</p>
										<p>condition for today's rating for activity<div class="star-rating" title="Rated 1 out of 5"><span style="width:60%"><strong class="rating">1</strong> out of 5</span></div>
									</div>
								</div>
								<div class="photo">
									<div class="photo-preview photo-detail" data-bg-image="images/photo-1.jpg"></div>
									<div class="photo-details">
									<div class="location">Kampong Pasir Ris</div>
									<div class="day">Monday</div>
									<div class="date">6 Oct</div>
									<div class="degree">
									<div class="num">23<sup>o</sup>C</div>
									<div class="forecast-icon">
										<img src="images/icons/icon-3.svg" alt="" width=48>
									</div>	
									</div>
										<p>please wear something appropiate based on the suggestion today.</p>
										<p>condition for today's rating for activity<div class="star-rating" title="Rated 1 out of 5"><span style="width:60%"><strong class="rating">1</strong> out of 5</span></div>
									</div>
								</div>
								<div class="photo">
									<div class="photo-preview photo-detail" data-bg-image="images/photo-1.jpg"></div>
									<div class="photo-details">
									<div class="location">Chye Kay</div>
									<div class="day">Monday</div>
									<div class="date">6 Oct</div>
									<div class="degree">
									<div class="num">23<sup>o</sup>C</div>
									<div class="forecast-icon">
										<img src="images/icons/icon-5.svg" alt="" width=48>
									</div>	
									</div>
										<p>please wear something appropiate based on the suggestion today.</p>
										<p>condition for today's rating for activity<div class="star-rating" title="Rated 1 out of 5"><span style="width:60%"><strong class="rating">1</strong> out of 5</span></div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="photo">
									<div class="photo-preview photo-detail" data-bg-image="images/photo-1.jpg"></div>
									<div class="photo-details">
									<div class="location">Bright Hill</div>
									<div class="day">Monday</div>
									<div class="date">6 Oct</div>
									<div class="degree">
									<div class="num">23<sup>o</sup>C</div>
									<div class="forecast-icon">
										<img src="images/icons/icon-1.svg" alt="" width=50>
									</div>	
									</div>
										<p>please wear something appropiate based on the suggestion today.</p>
										<p>condition for today's rating for activity<div class="star-rating" title="Rated 1 out of 5"><span style="width:60%"><strong class="rating">1</strong> out of 5</span></div>
									</div>
								</div>
								<div class="photo">
									<div class="photo-preview photo-detail" data-bg-image="images/photo-1.jpg"></div>
									<div class="photo-details">
									<div class="location">Woodlands</div>
									<div class="day">Monday</div>
									<div class="date">6 Oct</div>
									<div class="degree">
									<div class="num">23<sup>o</sup>C</div>
									<div class="forecast-icon">
										<img src="images/icons/icon-1.svg" alt="" width=50>
									</div>	
									</div>
										<p>please wear something appropiate based on the suggestion today.</p>
										<p>condition for today's rating for activity<div class="star-rating" title="Rated 1 out of 5"><span style="width:60%"><strong class="rating">1</strong> out of 5</span></div>
									</div>
								</div>
								<div class="photo">
									<div class="photo-preview photo-detail" data-bg-image="images/photo-1.jpg"></div>
									<div class="photo-details">
									<div class="location">Kampong Siren</div>
									<div class="day">Monday</div>
									<div class="date">6 Oct</div>
									<div class="degree">
									<div class="num">23<sup>o</sup>C</div>
									<div class="forecast-icon">
										<img src="images/icons/icon-1.svg" alt="" width=50>
									</div>	
									</div>
										<p>please wear something appropiate based on the suggestion today.</p>
										<p>condition for today's rating for activity<div class="star-rating" title="Rated 1 out of 5"><span style="width:60%"><strong class="rating">1</strong> out of 5</span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</main> <!-- .main-content -->

			<?php include 'include/footer/footer.php';?> <!-- .site-footer -->
		</div>
		
		<script src="<?php echo base_url('js/jquery-1.11.1.min.js'); ?>"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
		<script src="<?php echo base_url('js/plugins.js'); ?>"></script>
		<script src="<?php echo base_url('js/app.js'); ?>"></script>
		
	</body>

</html>