<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Login</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('fonts/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?php echo base_url('style.css'); ?>">
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>


	<body>
		
		<div class="site-content">
			<?php include 'include/login/header.php';?> <!-- .site-header -->

			<main class="main-content">

				<div class="fullwidth-block">
					<div class="container">
						
							<h2 class="section-title">Email Verification</h2>
							<p>Send otp code to email</p>
							<?php if(isset($validation)):?>
								<div class="alert alert-danger"><?= $validation->listErrors(); ?></div>
							<?php endif;?>
							<form action="/signup/save" class="contact-form" method="POST">

								<div class="row">
									<label for="uname"><b>E-Mail</b></label>
								</div>
								<div class="row">
									<div><input type="text" placeholder="example@mail.com" name = "email"></div>
								</div>


								<div class="row">
									<div><br><button type="submit">Send OTP</button></div>
								</div>
							</form>
							<br><br><br><br><br><br><br><br>
					</div>
				</div>
				
			</main> <!-- .main-content -->

			<?php include 'include/footer/footer.php';?> <!-- .site-footer -->
		</div>
		
		<script src="<?php echo base_url('js/jquery-1.11.1.min.js'); ?>"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
		<script src="<?php echo base_url('js/plugins.js'); ?>"></script>
		<script src="<?php echo base_url('js/app.js'); ?>"></script>
		
	</body>

</html>