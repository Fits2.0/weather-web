<!DOCTYPE html>
<html lang="en">
	<head>
	<style>
body {
  font-family: "Lato", sans-serif;
}

.sidenav {
	height: 100vh;
    width: 200px;
    float: left;
    position: relative;
    background-color: auto;
  
}
.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 15px;
  color: auto;
  display: block;
}



</style>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Weather It</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('fonts/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?php echo base_url('style.css'); ?>">
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>

	<body>
		
		<div class="site-content">
			<?php include 'include/user/header.php';?> <!-- .site-header -->

			<main class="main-content">
				<div class="container">
					<div class="breadcrumb">
						<a href="index.html">Home</a>
						
					</div>
				</div>
				<div class="fullwidth-block">
					<div class="container">
                        <?php include 'include/user/sidebar.php'; ?> <!-- side navigation -->

						<div class="col-md-6">
                            <? //echo "Welcome back, ".$session_('name');?>
							<h2 class="section-title">Current</h2>
							<p>This is your current account information.</p>
							<form action="#" class="contact-form">
								<div class="row">
								    <p>Name </p>
									<div class="col-md-6"><input type="text" placeholder="<?php echo $name; ?>" ></div>
								</div>
								<div class="row">
									<p> Email Address</p>
									<div class="col-md-6"><input type="text" placeholder="<?php echo $email; ?>" ></div>
								</div>
                                <div class="row">
									<p> City</p>
									<div class="col-md-6"><input type="text" placeholder="<?php echo $city;?>" ></div>
								</div>
							</form>
						</div>
					</div>
		
				</div>
			</main> <!-- .main-content -->

			<?php include 'include/footer/footer.php';?> <!-- .site-footer -->
		</div>
		<script src="js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		<script>
			function myFunction() {
			var x = document.getElementById("myDIV");
			if (x.style.display === "none") {
				x.style.display = "block";
			} else {
				x.style.display = "none";
			}
			}
		</script>
<script>

</script>

	</body>
</html>