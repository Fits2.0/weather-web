<!DOCTYPE html>
<html lang="en">
	<head>
	<style>
body {
  font-family: "Lato", sans-serif;
}

.sidenav {
	height: 100vh;
    width: 200px;
    float: left;
    position: relative;
    background-color: auto;
  
}
.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 15px;
  color: auto;
  display: block;
}

			.card {
                box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
                border-radius: 5px;
                padding: 1.4rem;
                width: 100%;
                margin-top: 0px;
                margin-right: auto;
                margin-bottom: 0px;
                margin-left: auto;
            }

            .card-pricing {
                box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
                background-color: lightgrey;
                border-radius: 5px;
                padding: 1.4rem;
                width: 100%;
                margin-top: 0px;
                margin-right: auto;
                margin-bottom: 0px;
                margin-left: auto;
            }

            .card-pricing-container {
                border-radius: 5px;
                background-color: white;
                padding: 1.4rem;
                width: 100%;
                margin-top: 0px;
                margin-right: auto;
                margin-bottom: 0px;
                margin-left: auto;
            }

            .card-content {
                border-radius: 5px;
                padding: 1.4rem;
                width: 100%;
                margin-top: 0px;
                margin-right: auto;
                margin-bottom: 0px;
                margin-left: auto;
            }
            .card__section-divider {
                border-top: 1px solid #d6d6d6;
                margin: 1.143rem 0;
            }
            .mb-m {
                margin-bottom: 1.6rem !important;
                margin-top: 1.6rem !important;
            }
            .container-form{    
                width: 50%;
            }



</style>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Weather It</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('fonts/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?php echo base_url('style.css'); ?>">
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>

	<body>
  <?php include 'include/logged_in.php';?> <!-- logged in user -->
		<div class="site-content">
			<?php include 'include/user/header.php';?> <!-- .site-header -->

    <?php
      //weather status data 
      $thunderstorm = 0;
      $drizzle = 0;
      $rain = 0;
      $snow = 0;
      $clear = 0;
      $cloud = 0;

			for($i = 0;$i < count($weather);$i++){
        $status = (int)$weather[$i]["weather_status_id"];
				if ($status >= 200 and $status < 300){
					$thunderstorm++;
				}
				else if ($status >= 300 and $status < 400){
					$drizzle++;
				}
				else if ($status >= 500 and $status < 600){
					$rain++;
        }
        else if ($status >= 600 and $status < 700){
					$snow++;
        }
        else if ($status == 800){
					$clear++;
        }
        else if ($status > 800 and $status < 900){
					$cloud++;
        }
      }

      //Average temperature for the month by city
      for($i = 0;$i < 7;$i++){
        //echo $kl[$i]["min_temperature"];
        $average_temperature_kl[] = (((double)$kl[$i]["min_temperature"] + (double)$kl[$i]["max_temperature"]) / 2);  
        $average_temperature_johor[] = (((double)$johor[$i]["min_temperature"] + (double)$johor[$i]["max_temperature"]) / 2);  
        $average_temperature_pineng[] = (((double)$pineng[$i]["min_temperature"] + (double)$pineng[$i]["max_temperature"]) / 2); 
        $date[] = $kl[$i]["calendar_date"];
      }

		?>
			<main class="main-content">
				<div class="fullwidth-block">
					<div class="container">
						<div class="col-md-9">
                            <? //echo "Welcome back, ".$session_('name');?>
							<h2 class="section-title">Weather report</h2>
							<!--p>This is your latest report for a week information.</p>-->
							 <div class="contact-form">
								<div class="card-pricing list-group list-group-flush row mb-m">
									<div class = "card-pricing-container row mb-m">
										<div class = "row">
											<!-- graph -->
											<div id = "container"></div>
										</div>
									</div>
								</div>
								<div class="card-pricing list-group list-group-flush row mb-m">
									<div class = "card-pricing-container row mb-m">
										<div class = "row">
											<!-- graph -->
											<div id="top_x_div" style = "height: 300px;"></div>
										</div>
									</div>
								</div>
							 </div>
						</div>

					</div>
				</div>
			</main> <!-- .main-content -->
			<?php include 'include/footer/footer.php';?> <!-- .site-footer -->
		</div>
		
		<script src="js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		
		<script>
			function myFunction() {
			var x = document.getElementById("myDIV");
			if (x.style.display === "none") {
				x.style.display = "block";
			} else {
				x.style.display = "none";
			}
			}
		</script>
		<script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart','line']});  
      </script>
	<script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Date');
            data.addColumn('number', 'Kuala Lumpur');
            data.addColumn('number', 'Johor');
            data.addColumn('number', 'Pulau Pinang');
            data.addRows([
               ['<?php echo date("Y:m:d", strtotime($date[0])); ?>',  <?php echo $average_temperature_kl[0]; ?>, <?php echo $average_temperature_johor[0]; ?>, <?php echo $average_temperature_pineng[0]; ?>],
               ['<?php echo date("Y:m:d", strtotime($date[1])); ?>',  <?php echo $average_temperature_kl[1]; ?>, <?php echo $average_temperature_johor[1]; ?>, <?php echo $average_temperature_pineng[1]; ?>],
               ['<?php echo date("Y:m:d", strtotime($date[2])); ?>',  <?php echo $average_temperature_kl[2]; ?>, <?php echo $average_temperature_johor[2]; ?>, <?php echo $average_temperature_pineng[2]; ?>],
               ['<?php echo date("Y:m:d", strtotime($date[3])); ?>',  <?php echo $average_temperature_kl[3]; ?>, <?php echo $average_temperature_johor[3]; ?>, <?php echo $average_temperature_pineng[3]; ?>],
               ['<?php echo date("Y:m:d", strtotime($date[4])); ?>',  <?php echo $average_temperature_kl[4]; ?>, <?php echo $average_temperature_johor[4]; ?>, <?php echo $average_temperature_pineng[4]; ?>],
               ['<?php echo date("Y:m:d", strtotime($date[5])); ?>',  <?php echo $average_temperature_kl[5]; ?>, <?php echo $average_temperature_johor[5]; ?>, <?php echo $average_temperature_pineng[5]; ?>],
               ['<?php echo date("Y:m:d", strtotime($date[6])); ?>',  <?php echo $average_temperature_kl[6]; ?>, <?php echo $average_temperature_johor[6]; ?>, <?php echo $average_temperature_pineng[6]; ?>]
            ]);
               
            // Set chart options
            var options = {'title' : 'Average Temperature for 3 cities in 1st week',
               hAxis: {
                  title: 'Date'
               },
               vAxis: {
                  title: 'Temperature'
               },   
			   width: '100%',
			   height: '100%'
            };

            // Instantiate and draw the chart.
            var chart = new google.visualization.LineChart(document.getElementById('container'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

	<script>
		google.charts.load('current', {'packages':['bar']});
      	google.charts.setOnLoadCallback(drawStuff);
		var thunderstorm = parseInt (<?php echo $thunderstorm; ?>);
		var drizzle = parseInt (<?php echo $drizzle; ?>);
    var snow = parseInt (<?php echo $snow; ?>);
    var rain = parseInt (<?php echo $rain; ?>);
    var clear = parseInt (<?php echo $clear; ?>);
    var cloud = parseInt (<?php echo $cloud; ?>);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Weather', 'count:'],
          ["Thunderstorm", thunderstorm],
          ["Drizzle", drizzle],
          ["Rain", rain],
          ["Snow", snow],
          ["Clear", clear],
          ["Cloudy", cloud]
        ]);

        var options = {
          title: 'Weather status of 3 cities in 1st week',
          width: '90%',
          height: '100%',
          backgroundColor: 'transparent',
          legend: { position: 'none' },
          chart: { title: 'Weather status of of 3 cities in Malaysia in 1st week',
                   subtitle: 'Weather count' },
          bars: 'horizontal', // Required for Material Bar Charts.
          backgroundColor: '#f1f8e9',
          axes: {
            x: {
              0: { side: 'top', label: 'Weather status count'} // Top x-axis.
            }
          },
		     bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        chart.draw(data, options);
      };
	</script>
	</body>
</html>