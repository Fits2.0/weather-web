<!DOCTYPE html>
<html lang="en">
	<?php 
	/*if ([session('userData.name')] != ''){
		echo 'this user is logged in';	
	}
	else {
		echo 'this user is not logged in';
	}*/
		?>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Weather It</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url('fonts/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="<?php echo base_url('style.css'); ?>">
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->
		
	</head>

	<body onload="myFunction()">
	<?php include 'include/logged_in.php';?> <!-- logged in user -->
		<div class="site-content">
			<?php include 'include/home/header.php';?>
			<div class="hero" data-bg-image="images/cities/head-02.jpg">
				<div class="container">
					<form action="#" class="find-location">
						<input type="text" placeholder="Find your location...">
						<input type="submit" value="Find">
					</form>

				</div>
			</div>
			<div class="forecast-table">
				<div class="container">
					<div class="forecast-container">
						<div class="today forecast">
							<div class="forecast-header">
								<div class="day"><?php echo date("l", strtotime($weather[0]["last_updated_at"])); ?></div>
								<div class="date"><?php echo date("jS \of F"); ?></div>
							</div> <!-- .forecast-header -->
							<div class="forecast-content" style = "align-content: center;">
								<div class="location"><?php echo $weather[0]["city_name"]; ?></div>
								<div class="degree">
									<div class="num"><?php echo (int)$weather[0]["temperature"]; ?><sup>o</sup>C</div>
									<div class="forecast-icon">
										<img src="images/icons/icon-3.svg" alt="" width=90>
									</div>	
								</div>
								<span><img src="images/icon-umberella.png" alt="" ><?php echo (int)$weather[0]["humidity"]; ?>%</span>
								<span><img src="images/icon-sunrise.png" alt="" width = "30px" height = "30px"><?php echo date("g:i a", strtotime($weather[0]["sunrise_time"])); ?></span>
								<span><img src="images/icon-sunset.png" alt="" width = "30px" height = "30px"><?php echo date("g:i a", strtotime($weather[0]["sunset_time"])); ?></span>
							</div>
						</div>
						<div class="today forecast">
							<div class="forecast-header">
								<div class="day"><?php echo date("l", strtotime($weather[1]["last_updated_at"])); ?></div>
								<div class="date"><?php echo date("jS \of F"); ?></div>
							</div> <!-- .forecast-header -->
							<div class="forecast-content" style = "align-content: center;">
								<div class="location"><?php echo $weather[1]["city_name"]; ?></div>
								<div class="degree">
									<div class="num"><?php echo (int)$weather[1]["temperature"]; ?><sup>o</sup>C</div>
									<div class="forecast-icon">
										<img src="images/icons/icon-3.svg" alt="" width=90>
									</div>	
								</div>
								<span><img src="images/icon-umberella.png" alt="" ><?php echo (int)$weather[1]["humidity"]; ?>%</span>
								<span><img src="images/icon-sunrise.png" alt="" width = "30px" height = "30px"><?php echo date("g:i a", strtotime($weather[1]["sunrise_time"])); ?></span>
								<span><img src="images/icon-sunset.png" alt="" width = "30px" height = "30px"><?php echo date("g:i a", strtotime($weather[1]["sunset_time"])); ?></span>
							</div>
						</div>
						<div class="today forecast">
							<div class="forecast-header">
								<div class="day"><?php echo date("l", strtotime($weather[2]["last_updated_at"])); ?></div>
								<div class="date"><?php echo date("jS \of F"); ?></div>
							</div> <!-- .forecast-header -->
							<div class="forecast-content" style = "align-content: center;">
								<div class="location"><?php echo $weather[2]["city_name"]; ?></div>
								<div class="degree">
									<div class="num"><?php echo (int)$weather[2]["temperature"]; ?><sup>o</sup>C</div>
									<div class="forecast-icon">
										<img src="images/icons/icon-3.svg" alt="" width=90>
									</div>	
								</div>
								<span><img src="images/icon-umberella.png" alt="" ><?php echo (int)$weather[2]["humidity"]; ?>%</span>
								<span><img src="images/icon-sunrise.png" alt="" width = "30px" height = "30px"><?php echo date("g:i a", strtotime($weather[2]["sunrise_time"])); ?></span>
								<span><img src="images/icon-sunset.png" alt="" width = "30px" height = "30px"><?php echo date("g:i a", strtotime($weather[2]["sunset_time"])); ?></span>
							</div>
						</div>
						<!--
						<div class="forecast">
							<div class="forecast-header">
								<div class="day">Thursday</div>
							</div>
							<div class="forecast-content">
								<div class="forecast-icon">
									<img src="images/icons/icon-7.svg" alt="" width=48>
								</div>
								<div class="degree">23<sup>o</sup>C</div>
								<small>18<sup>o</sup></small>
							</div>
						</div>
						<div class="forecast">
							<div class="forecast-header">
								<div class="day">Friday</div>
							</div>
							<div class="forecast-content">
								<div class="forecast-icon">
									<img src="images/icons/icon-12.svg" alt="" width=48>
								</div>
								<div class="degree">23<sup>o</sup>C</div>
								<small>18<sup>o</sup></small>
							</div>
						</div>
						<div class="forecast">
							<div class="forecast-header">
								<div class="day">Saturday</div>
							</div>
							<div class="forecast-content">
								<div class="forecast-icon">
									<img src="images/icons/icon-13.svg" alt="" width=48>
								</div>
								<div class="degree">23<sup>o</sup>C</div>
								<small>18<sup>o</sup></small>
							</div>
						</div>
						<div class="forecast">
							<div class="forecast-header">
								<div class="day">Sunday</div>
							</div>
							<div class="forecast-content">
								<div class="forecast-icon">
									<img src="images/icons/icon-14.svg" alt="" width=48>
								</div>
								<div class="degree">23<sup>o</sup>C</div>
								<small>18<sup>o</sup></small>
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
			<main class="main-content">
				<div class="fullwidth-block">
					<div class="container">
						<h2 class="section-title">Cities</h2>
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<div class="live-camera">
									<img src="<?php echo base_url('images/home/cities-01.jpg') ?>" alt="" width="100%" height="250px"></figure>
									<h3 class="location">Penang</h3>
								</div>
							</div>
							<div class="col-md-4 col-sm-6">
								<div class="live-camera">
									<img src="<?php echo base_url('images/home/cities-02.jpg') ?>" alt="" width="100%" height="250px"></figure>
									<h3 class="location">Kuala Lumpur</h3>
								</div>
							</div>
							<div class="col-md-4 col-sm-6">
								<div class="live-camera">
									<img src="<?php echo base_url('images/home/cities-03.jpg') ?>" alt="" width="100%" height="250px"></figure>
									<h3 class="location">Johor</h3>
								</div>
							</div>
						</div>
					</div>
				</div>

			

				<div class="fullwidth-block">
					<div class="container">
						<h2 class="section-title">Application Features</h2>
						<div class="row">
							<div class="col-md-3 col-sm-6">
								<div class="live-camera">
									<img src="images/live-camera-14.jpg" alt="" width="259" height="194"></figure>
									<h3>Daily weather forecast</h3>
									
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="live-camera">
									<img src="images/live-camera-15.jpg" alt="" width="259" height="194"></figure>
									<h3>Hourly weather forecast</h3>
									
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="live-camera">
									<img src="images/live-camera-16.jpg" alt="" width="259" height="194"></figure>
									<h3>Details of each states weather updates</h3>
									
								</div>
							</div>

						</div>
					</div>
				</div>
			</main> <!-- .main-content -->

			<?php include 'include/footer/footer.php';?> <!-- .site-footer -->
		</div>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-MKWEK3F22T"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-MKWEK3F22T');
		</script>
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		<script>
			var url = window.location
			var baseurl = url.protocol + '//' + url.host
			var updateC = baseurl + '/presto/1'// call php url for update
			var updateH = baseurl + '/presto/2'//Hourly
			var updateD = baseurl + '/presto/3'//Daily
			var lastH = baseurl + '/presto/4' // get last hour forecast
			var lastD = baseurl + '/presto/5'
			window.onload = function() {
				updateCurrent()//Update current weather function
				updateHourly()//Update hourly forecast function
				updateDaily()// Update daily forecast function
					
			}
			var hour = 24 * 60 * 60 *10000;
			
			function updateCurrent(){
				$.get(updateC);
			}
			function updateHourly(){
				$.getJSON(lastH,function(data){
				ts = Math.floor(Date.now()/1000)
				if(ts == data.time){
					$.get(updateH)
				}
			})
			}
			function updateDaily(){
				$.getJSON(lastD,function(data){

				
				ts = Math.flood(Date.now()/1000)
				if(ts == data.date){
					$.get(updateD)
				}	
			})
			}

			setInterval(function() {
				$.get(update, function(data) {
				//do something with the data

				alert(data)
				});
				//alert('Load was performed.');
			}, hour);
		</script>
	</body>

</html>

