<style>
.navbar {
  width: 100%;
  overflow: auto;
}

.navbar a {
  float: left;
  padding: 12px;
  color: white;
  text-decoration: none;
  font-size: 14px;
  width: 33%; /* Four links of equal widths */
  text-align: center;
}

.navbar a:hover {
  background-color: #000;
}



@media screen and (max-width: 500px) {
  .navbar a {
    float: none;
    display: block;
    width: 100%;
    text-align: left;
  }
}


</style>

<div id="mynavbar"  class="navbar">
    <a href="../current-weather">Current update</a>
    <a href="../weather">Daily update</a>
    <a href="../hourly">Hourly update</a>
</div>