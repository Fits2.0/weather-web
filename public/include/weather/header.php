<div class="site-header">
				<div class="container">
					<a href="../home" class="branding">
						<img src="<?php echo base_url('images/logo.png'); ?>" alt="" class="logo">
						<div class="logo-type">
							<h1 class="site-title">Weather It </h1>
							<small class="site-description">The best weather updates for you</small>
						</div>
					</a>

					<!-- Default snippet for navigation -->
					<div class="main-navigation">
						<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
						<ul class="menu">
							<li class="menu-item"><a href="../home">Home</a></li>
							<li class="menu-item"><a href="../report">Report</a></li>
							<li class="menu-item current-menu-item"><a href="../weather">Weather</a></li>
							<li class="menu-item"><a href="../login">Login</a></li>
						</ul> <!-- .menu -->
					</div> <!-- .main-navigation -->

					<div class="mobile-navigation"></div>

				</div>
			</div>  <!-- .site-header -->